# Sorting Algorithms: Quick-Sort Advanced

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

In the previous challange ([7.10. Simple Quick Sort](https://repl.it/student/submissions/7784058)), we solved the problem by using a brute-force partitioning algorithm. However, this is not the most efficient. We will optimize the partitioning method in this challenge.

A quick reminder of the algorithm: quick sort is one of the most efficient sorting algorithms and is based on a divide-and-conquer approach.

The steps are:<br>
&emsp;\- **Divide:** Partition the array into two sub-arrays such that the elements in the lower part are smaller than the elements in the higher part.<br>
&emsp;\- **Conquer:** Sort the two sub-arrays recursively.<br>
&emsp;\- **Combine:** Trivial, because the sub-arrays are already sorted. No additional work is needed to combine them.<br>

First, let's take a look at what we have.

A quick-sort algorithm can be implemented simply as:<br>
```ruby
# we'll use begin_index and end_index for dividing the problem.
def quick_sort(array, begin_index = 0, end_index = array.length - 1)
  # divide the problem until there's a sub-array between the begin and the end indices
  if begin_index < end_index
    # partition the array
    pivot_index = partition(array, begin_index, end_index)
    # solve the sub-problem that includes the elements smaller than the pivot
    quick_sort(array, begin_index, pivot - 1)
    # solve the sub-problem that includes the elements greater than the pivot
    quick_sort(array, pivot + 1, end_index)
  end
end
```

And the partition function can be implemented as:<br>
```ruby
def partition(array, begin_index, end_index)
  # pick the first element as the pivot
  pivot = array[begin_index]
  # holds the elements smaller than the pivot
  smaller = []
  # holds the elements greater than the pivot
  greater = []
  # iterate over the array except the first element 
  array[begin_index + 1..end_index].each do |e|
    # if the current element is smaller than the pivot,
    # push it into the _smaller_ array
    if e < pivot
      smaller << e
    # otherwise, push it into the _greater_ array
    else
      greater << e
    end
  end
  # now, update the current part
  array[begin_index..end_index] = (smaller + [pivot] + greater)
  # because the pivot is placed after the _smaller_ array, its index starts after the _smaller_ array
  begin_index + smaller.length
end
```

As you can see, this produces new arrays (smaller and greater) each time we partition. It runs with time complexity O(n), and no improvement is needed. However, we can use an efficient algorithm to reduce its **space complexity**.

Space complexity, also called location complexity, is the amount of working storage an algorithm needs.

We will use an **in-place algorithm** which transforms the input without using an auxiliary data structure. Essentially, we manipulate only the **base array** instead of using the _smaller_ and _greater_ arrays.

There are two canonical algorithms for partitioning an array: **Hoare and Lomuto partition schemes**. Hoare’s algorithm is generally faster, but we will implement Lomuto's algorithm, because the latter is easier to implement and understand.

The Lomuto partition follows the same rules as what we have already done, except now we will take the last element as the pivot:<br>
&emsp;\- take the last element as the pivot.<br>
&emsp;\- place all smaller elements (smaller than the pivot) to the left of the pivot.<br>
&emsp;\- place all greater elements (greater than the pivot) to the right of the pivot.<br>
&emsp;\- place the pivot in its correct position in the sorted array.<br>

<img src="images/sorting-algorithms/quick-sort-advanced/quick-sort-advanced-1.png"  width="800"><br>

<b>`Hint:`&nbsp;You need two indices: one for the basic iteration over the array, and another for keeping track of the starting position of the greater group of elements.<br>
<b>`Hint:`&nbsp;Iterate over the array excluding the pivot index. When the current element is smaller than the pivot, swap the current element with the first greater element (if there is one).<br>

<b>`Note:`</b>&nbsp;Ruby provides an easy way to swap two values: `array[i], array[j] = array[j], array[i]`<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;Flow of the partitioning algorithm</summary><br>

The image below shows the first three steps of the partition algorithm for the array [7, 8, 2, 6, 5, 1, 3, 4]:<br>
<img src="images/sorting-algorithms/quick-sort-advanced/quick-sort-advanced-2.png"  width="800"><br>

---

When you finish the iteration, the pivot is still at the end of the array. We need one more swap. Swap the last element with the starting element in the group of greater values.<br>
<img src="images/sorting-algorithms/quick-sort-advanced/quick-sort-advanced-3.png"  width="800"><br>

<b>`Hint:`&nbsp;If the current element is greater than the pivot, continue the iteration.<br>
<b>`Hint:`&nbsp;If the current element is smaller than the pivot, swap the current element with the first greater element (memorized).<br>
<b>`Hint:`&nbsp;After each swap, update the starting index of the greater values. Incrementing it by 1 is sufficient.<br>
<b>`Hint:`&nbsp;After the iteration above, the pivot is still at the end of the array. Swap it with the greater element.<br>

</details>
<!-- Hint 2 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>

```ruby
def partition(array, begin_index, end_index)
  # take the last element as the pivot
  pivot = array[end_index]
  # use another index for holding the greater value
  # use the first element as the initial greater value
  j = begin_index
  for i in begin_index...end_index
    if array[i] < pivot
      # swap the current element with the memorized greater element
      array[i], array[j] = array[j], array[i]
      # increment the greater index
      j += 1
    end
  end
  # the current placement will be [smaller elements, greater elements, pivot]
  # then you need to swap the pivot with the last element of the greater group
  array[end_index], array[j] = array[j], array[end_index]
  # return the index of the pivot
  j
end

def advanced_quicksort(array, begin_index = 0, end_index = array.length - 1)
  # divide the problem until there's a sub-array between the begin and end indices
  if begin_index < end_index
    # partition the array
    pivot = partition(array, begin_index, end_index)
    # print the resulting array after each partition
    puts "#{array}"
    # solve the sub-problem that includes the elements smaller than the pivot
    advanced_quicksort(array, begin_index, pivot - 1)
    advanced_quicksort(array, pivot + 1, end_index)
  end
end
```
</details>
<!-- Solution -->

